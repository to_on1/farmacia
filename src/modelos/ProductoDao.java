/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;

/**
 *
 * @author antonio
 */
public class ProductoDao extends Sesion{
    
    public Integer guardarProducto(Producto producto) throws HibernateException
    {
        int id=0;

        try {
            iniciarOperacion();
            id = (Integer)session.save(producto);
            tx.commit();
        } catch (HibernateException he) {
            coneccionExcepcion(he, "ProductoDao");
            throw he;
        } finally{
            session.close();
        }
        return id;
    }
    
    public void actualizarProducto(Producto producto) throws HibernateException
    {
        try {
            iniciarOperacion();
            session.update(producto);
            tx.commit();
        } catch( HibernateException he){
            coneccionExcepcion(he, "ProductoDao");
            throw he;
        } finally{
            session.close();
        }
    }
    public void eliminarProducto(Producto producto) throws HibernateException{
        try {
            iniciarOperacion();
            session.delete(producto);
            tx.commit();
        } catch (HibernateException he) {
            coneccionExcepcion(he, "Producto Dao");
            throw he;
        } finally {
            session.close();
        }
    }
    public Producto obtenerProductoPorId(int id) {
        Producto producto = null;
        try {
            iniciarOperacion();
            producto = (Producto) session.get(Producto.class, id);
        } finally {
            session.close();
        }
        return producto;
    }
    
    public List<Producto> obtenerProductos() {
        List<Producto> productos = new ArrayList<Producto>();
        try {
            iniciarOperacion();
            Query q = session.createQuery("FROM Producto");
            productos = q.list();
        } finally {
            session.close();
        }
        return productos;
    }
}
