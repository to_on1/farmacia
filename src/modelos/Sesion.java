/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author antonio
 */
public class Sesion {
    
    public Session session;
    public Transaction tx;
    
    public void iniciarOperacion() throws HibernateException
    {
        session = HibernateUtil.getSessionFactory().openSession();
        tx = session.beginTransaction();
    }
    
    public void coneccionExcepcion(HibernateException he, String clase) 
            throws HibernateException
    {
        tx.rollback();
        throw new HibernateException("Ups!! Ocurrio un error en la capa de "
                + "acceso de datos en la clase "+clase+"\n"+he);
    }
}
