package modelos;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    private static final SessionFactory sessionFactory;
    static{
        try {
            sessionFactory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable he) {
            System.err.println("La creacion de sessionFactory fallo "+he);
            throw new ExceptionInInitializerError(he);
        }
    }
    public static SessionFactory getSessionFactory(){
        return sessionFactory;
    }
}
