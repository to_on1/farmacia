/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

/**
 *
 * @author antonio
 */
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="producto")
public class Producto implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    
    private String nombre;
    
    @Column(name="fecha_venciemiento")
    @Temporal(TemporalType.DATE)
    private Date fechaVen;
    
    public Producto(){
    }
    
    public Producto(String nombre, Date fechaVen){
        this.nombre = nombre;
        this.fechaVen = fechaVen;
    }
    
    public int getId(){
        return this.id;
    }
    
    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    
    public String getNombre(){
        return nombre;
    }
    
    public Date getFechaVencimiento(){
        return fechaVen;
    }
}
