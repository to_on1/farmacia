/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import com.toedter.calendar.JTextFieldDateEditor;
import com.toedter.calendar.JDateChooser;
import modelos.Producto;
import modelos.ProductoDao;

/**
 *
 * @author antonio
 */
public class Registro extends JPanel {
    
    private JButton botonGuardar;
    
    private JTextField textNombre, textFecha;
    
    private JLabel lnombre, lfecha;
    
    private GridLayout gridl;
    
    private JTextFieldDateEditor calendar;
    private JDateChooser calendar2;
    
    public Registro(){
        gridl = new GridLayout(3, 2);        
        //panel.setBackground(Color.BLUE);
        setLayout(null);
        //setBounds(10, 10, 400, 100);
        setSize(200,200);
        crearTextField();
        crearBoton();
        setVisible(true);
    }
    
    
    public void crearTextField(){
        
        lnombre = new JLabel("Nombre producto:");
        lnombre.setBounds(10, 10, 200, 20);
        textNombre = new JTextField(100);
        textNombre.setBounds(210, 10, 200, 20);
        lfecha = new JLabel("Fecha Vencimiento:");
        lfecha.setBounds(10, 40, 200, 20);
        textFecha = new JTextField(100);
        //textFecha.setBounds(210, 40, 200,20);
        
        //calendar = new JTextFieldDateEditor();
        calendar2 = new JDateChooser();
        calendar2.setBounds(210, 40, 200,20);
        add(lnombre);
        add(textNombre);
        add(lfecha);
        //add(textFecha);
        add(calendar2);
        repaint();
        
    }
    
    public void crearBoton(){
        botonGuardar = new JButton();
        botonGuardar.setText("Guardar");
        botonGuardar.setBounds(70, 65, 200, 20);
        botonGuardar.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent ev){
                System.out.println("El texto introducido es:"+textNombre.getText());
                ProductoDao prodao = new ProductoDao();
                Producto p = new Producto(textNombre.getText(), calendar2.getDate());
                prodao.guardarProducto(p);
                repaint();
            }
        });
        add(botonGuardar);
        //panel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        repaint();
    }
}
