/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelos.Producto;
import modelos.ProductoDao;

/**
 *
 * @author antonio
 */
public class ListaProducto extends JPanel {
    private JTable tabla;
    
    private JScrollPane scroll;
    
    private JButton actualizar;
    
    public ListaProducto(){
        setSize(400, 400);
        llenarTabla();
        boton();
        setVisible(true);
    }
    public void llenarTabla(){
        DefaultTableModel dtm= new DefaultTableModel();

        ProductoDao prod = new ProductoDao();
        List<Producto> productos = prod.obtenerProductos();
//        Object[][] cellData = { { "1-1", "1-2" }, { "2-1", "2-2" } };
        Object [] data = new Object[2];
        String[] columnNames = {"Nombre", "Fecha"};
        dtm.addColumn("Nombre");
        dtm.addColumn("Fecha");
        for (Producto p : productos) {
            data[0]=p.getNombre();
            data[1]=p.getFechaVencimiento();
            dtm.addRow(data);
        }
        tabla = new JTable(dtm);
        
        scroll =new JScrollPane(tabla);
        add(scroll);
    }
    public void boton(){
        actualizar = new JButton("Actualizar");
        add(actualizar);
        actualizar.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent ev){
                scroll.repaint();
                updateUI();
                repaint();
            }
        });
    }
}
