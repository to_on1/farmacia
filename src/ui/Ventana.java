/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import modelos.Producto;
import modelos.ProductoDao;
/**
 *
 * @author antonio
 */
public class Ventana{
    
    private Dimension dimencion;
    
    private JFrame frame;
    
    private GridLayout gridl, gridl2;

    private FlowLayout flowl;
    
    private JButton actualizar;
    
    public Ventana(){
        dimencion = Toolkit.getDefaultToolkit().getScreenSize();
        inicializar();
        //crearTextField();
        //crearBoton();
    }
    
    public void inicializar(){
        frame = new JFrame();
        frame.setSize(dimencion); 
    //    frame.getContentPane().setLayout(null);
        frame.setBackground(Color.red);
        frame.setTitle("Farmacia Ucatec");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        gridl = new GridLayout(1, 1);
        flowl = new FlowLayout();
        frame.setLayout(gridl);
        /*
        panel = new JPanel(new BorderLayout());
        //panel.setBackground(Color.BLUE);
        panel.setLayout(gridl);
        panel.setBounds(10, 10, 400, 100);
        //frame.setLayout(flowl);
        */
        frame.add(new Registro());
        frame.add(new ListaProducto());
        frame.setVisible(true);
    }
    public void update(){
        actualizar = new JButton("Actualizar");
        frame.add(actualizar);
        actualizar.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent ev){
                
            }
        });
    }
}
