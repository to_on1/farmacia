/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

/**
 *
 * @author antonio
 */
import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class Ejemplo extends JFrame{
        private JTable tabla;
        private JScrollPane scroll;
        private JPanel panel;
	public Ejemplo(){
		setTitle("Ejemplo 1");
		setLayout(null);
		setSize(500,500);
		setLocation( 200,150 );
		getContentPane().setBackground(Color.blue);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                crearTabla();
	}
        public void crearTabla(){
            Object[][] cellData = { { "1-1", "1-2" }, { "2-1", "2-2" } };
            String[] columnNames = { "col1", "col2" };
            tabla = new JTable(cellData, columnNames);
            scroll =new JScrollPane(tabla);
            panel = new JPanel();
            panel.add(scroll);
            panel.setBounds(10, 10, 400, 400);
            panel.setVisible(true);
            add(panel);
            repaint();
        }
	public static void main( String[] args ){
		Ejemplo ej = new Ejemplo();
		ej.setVisible(true);
	}
}